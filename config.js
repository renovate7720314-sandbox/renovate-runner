module.exports = {
  autodiscover: true,
  autodiscoverFilter: ["auto7770514/{feature,docker-auto}"],
  baseBranches: ["feature/poc_renovate"],
};